# Docker Compose project WALL-e monitoring

This an example project to show the TIG (Telegraf, InfluxDB and Grafana) stack.

![Example Screenshot](./pics/grafana_monitor.png?raw=true "Example Screenshot")

![Example Screenshot](./pics/monitoring_three_systems.drawio.png?raw=true "Example Screenshot")

![Example Screenshot](./pics/terminal.png?raw=true "Example Screenshot")

## Start the stack with docker compose

```bash
$ docker-compose up
```
